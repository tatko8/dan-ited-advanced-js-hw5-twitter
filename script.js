class Card {
  constructor(title, text, user) {
    this.title = title;
    this.text = text;
    this.user = user;
    this.id = null;
    this.card = null;
  }

  render() {
    const card = document.createElement("div");
    card.classList.add("card");

    const cardUser = `<h2 class="card-user">${this.user.name}</h2>`;
    const cardUsermail = `<a href="mailto:${this.user.email}">${this.user.email}</a>`;
    const cardTitle = `<p class="card-title">${this.title}</p>`;
    const cardText = `<p class="card-text">${this.text}</p>`;
    const deleteButton = `<button class="delete-button">Видалити</button>`;

    card.innerHTML = cardUser + cardUsermail + cardTitle + cardText + deleteButton;

    card.querySelector(".delete-button").addEventListener("click", () => {
      this.delete();
    });
    this.card = card;
    return card;
  }

  delete() {
    if (this.id) {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
        method: "DELETE",
      })
        .then((response) => {
          if (response.ok) {
            this.card.remove();
          } else {
            console.error("При видаленні сталася помилка");
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }
}

fetch("https://ajax.test-danit.com/api/json/users")
  .then((response) => response.json())
  .then((users) => {
    fetch("https://ajax.test-danit.com/api/json/posts")
      .then((response) => response.json())
      .then((posts) => {
        const cardsContainer = document.createElement("div");
        cardsContainer.classList.add("cards-container");
        document.body.append(cardsContainer);

        posts.forEach((post) => {
          const user = users.find((u) => u.id === post.userId);
          const card = new Card(post.title, post.body, user);
          card.id = post.id;
          cardsContainer.appendChild(card.render());
        });
      })
      .catch((error) => {
        console.error(error);
      });
  })
  .catch((error) => {
    console.error(error);
  });
